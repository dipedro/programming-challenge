# Desafio para o Módulo Chatbot

## Objetivos Principais
- Solicitamos que utilize o MongoDB como banco de dados. :white_check_mark:
- Consertar os erros críticos já existentes na aplicação; :white_check_mark:
- Aprimorar a legibilidade da aplicação; :white_check_mark:
- O gerenciamento de dados externos da aplicação deve sempre ser intermediado pelo back-end; :white_check_mark:
- Simule a realização do atendimento através do clique de um botão;
- Ter os requisitos básicos já descritos; :white_check_mark:
- Aplicação de princípios RESTful. :white_check_mark:

## Objetivos Opcionais

1. Um sistema de login que possa diferenciar atendente e cliente; :white_check_mark:
2. Apresentar um FAQ para o cliente; :white_check_mark:
3. Utilizar os dados pessoais salvos do cliente para que seja solicitado apenas o assunto no próximo atendimento; :white_check_mark:
4. Suporte para que o atendimento possa ser realizado por diferentes atendentes;
5. Diferenciação entre conversas iniciadas e finalizadas; :white_check_mark:
6. Troca de mensagens em tempo real entre o cliente e o atendente; :white_check_mark:
7. Desenvolver para diferentes ambientes (local, homologação e produção);
8. Testes de código;
9. Responsividade do layout em diferentes resoluções; :white_check_mark:
10. Aplicação de princípios básicos de usabilidade no front-end. :white_check_mark:

# Frontend SAC Mateus

Frontend da aplicação SAC Mateus.

<p align="center">
  <img alt="Img SAC Mateus" src="https://i.imgur.com/7plCE48.png" width="100%">
  <img alt="Atendimento SAC Mateus" src="https://i.imgur.com/x5AoqkX.png" width="100%">
</p>

### Tecnologias
TypeScript, ReactJS e Socket.io

## Instalação

Clone o repositório, entre na pasta do projeto clonado e digite o seguinte comando, caso esteja utilizando o npm:

```bash
npm install
```

## Execução

Digite o seguinte comando, caso esteja utilizando o npm:

```bash
~ npm start

# Aguarde até aparecer a seguinte mensagem:
~ You can now view front-end in the browser.       

  Local:            http://localhost:3000 
```

# Backend

Backend da aplicação SAC Mateus.

### Tecnologias
NodeJS, MongoDB (utilizei o banco grátis do MongoDBAtlas) e Socket.io

## Instalação

Clone o repositório, entre na pasta do projeto clonado e digite o seguinte comando:

```bash
npm install
```

## Execução

Digite o seguinte comando caso esteja utilizando o npm.

```bash
~ npm start

# Aguarde até aparecer a seguinte mensagem:
~ Server is running on port 3001.
```

## Rotas

```javascript
routes.get('/');

routes.post('/auth');

routes.post('/clients');

routes.post('/attendants');

routes.get('/faqs');
routes.post('/faqs');

routes.get('/subjects');
routes.post('/subjects');

routes.get('/tickets');
routes.get('/tickets/:id');
routes.post('/tickets');
routes.put('/tickets');

routes.get('/conversations/:id');
routes.post('/conversations');

routes.get('/users');
routes.post('/users');

```




## License
[MIT](https://choosealicense.com/licenses/mit/)
